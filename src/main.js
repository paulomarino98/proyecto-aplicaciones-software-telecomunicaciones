const { app, BrowserWindow, BrowserView, Menu } = require('electron');
//const { createMenu } = require('./js/menu');
const path = require('path');

require('electron-reload')(__dirname, {
});

let ventana_informacion;




// cargamos la aplicacion 
function loadApp () {
    //Menu.setApplicationMenu(createMenu());
    const window = new BrowserWindow({
        show: false,
        title: 'Finanzas App',
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        }
    });
    // window.loadFile(path.join(__dirname, 'views/home.html'));
    window.loadFile(path.join(__dirname, 'views/home.html'));
    window.maximize();
    // window.webContents.openDevTools();
    window.show();
    const menuprincipal = Menu.buildFromTemplate(templateMenu)
    Menu.setApplicationMenu(menuprincipal);
}

const templateMenu = [
    {
        label:'Opciones', 
        submenu:[
            {
                label:'Reportar un problema',
                accelerator: 'Ctrl+P',
                
            },
            {
                label:'Salir',
                accelerator: process.platform =='darwin' ? "command+Q": "Ctrl+Q",
                click(){
                    app.quit();
                }
            }
        ],
    },
    
    {
        label:'Ver', 
        submenu:[
            {
                label:'Mi reporte',
                accelerator: 'Ctrl+M',
            },
        ],
    },
];
if(!app.isPackaged){
    templateMenu.push({
        label: "Tools",
        submenu: [
            {
                label: 'Mostrar/Ocultar DevTools',
                click(item, focusedWindow){
                    focusedWindow.toggleDevTools();
                },
            },
            {
                role: "reload"
            },
        ],
    });
}

app.whenReady().then(() => {
    loadApp();
});