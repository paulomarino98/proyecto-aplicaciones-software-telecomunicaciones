const fs = require('fs');
const path = require('path');

const ruta_archivo = path.join(__dirname, '../data/datos.json');

/**
 * 
 * @returns la data cargada desde el archivo json
 */
export const cargar_data = async () => {
    if (fs.existsSync(ruta_archivo)) {
        const archivo = fs.readFileSync(ruta_archivo);
        const data = JSON.parse(archivo);
        return data;
    }
}

/**
 * 
 * @param {Array} data los usuarios registrados en el sistema 
 */
export const guardar_data = async (data) => {
    fs.writeFileSync(ruta_archivo, JSON.stringify(data));
}