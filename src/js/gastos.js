import { cargar_data, guardar_data } from './data.js'; //cargamos los datos de las librerias data .js 

const data = await cargar_data();
const params = new URLSearchParams(window.location.href.split('?')[1]);
const id = Number.parseInt(params.get('id'));
const user = data.users[id];

let opcion = 'editar';
let numca = 0;
let numga = 0;

const links = document.getElementsByClassName('link');

for (const link of links) {
    link.href += `?id=${ id }`;
}

const selectOpcion = document.getElementById('select-opcion');
const selectCategorias = document.getElementsByClassName('categorias');
const selectGastos = document.getElementById('select-gastos');
const content = document.getElementById('content');

const updateContent = () => { //actualizar la pagina de los gastos  con todos los parametros vacios
    if (user.categories[numca].gastos.length > 0) {
        if (opcion.localeCompare('editar') === 0) {
            content.innerHTML = `<h2>Ingrese la descripción del gasto a registrar:</h2> <br>
            <input type="text" placeholder="Descripción" id="nuevo-nombre-gasto"><br><br><br>
            <h2>Ingrese el monto del gasto:</h2> <br>
            <input type="number" placeholder="Monto" id="nuevo-monto-gasto"><br><br><br>
            <h2>Ingrese fecha del gasto:</h2> <br>
            <input type="text" placeholder="Fecha" id="nuevo-fecha-gasto"><br><br><br>
            <button type="button" class="submit" id="editar">Editar Gasto</button><br><br><br><br>`;
            document.querySelector('#editar').onclick = editar;
        } else {
            content.innerHTML = `<button type="button" class="submit" id="eliminar">Eliminar Gasto</button>`;
            document.querySelector('#eliminar').onclick = eliminar;
        }
    } else {
        content.innerHTML = '<p>No hay gastos para mostrar.</p>'
    }
}

const updateSelectGastos = () => { 
    selectGastos.innerHTML = '';
    user.categories[numca].gastos.forEach((g, i) => {
        const row = `<option value=${i}>${g.descripcion}</option>`;
        selectGastos.innerHTML += row;
    });
}

const updateInputGastos = () => {
    const gasto = user.categories[numca].gastos[numga];
    if (gasto) {
        document.querySelector('#nuevo-nombre-gasto').value = gasto.descripcion;
        document.querySelector('#nuevo-monto-gasto').value = gasto.valor;
        document.querySelector('#nuevo-fecha-gasto').value = gasto.fecha;
    }
}

for (const sc of selectCategorias) {
    user.categories.forEach((c, i) => {
        if (c.name.localeCompare('default') !== 0) {
            const row = `<option value=${ i }>${ c.name }</option>`;
            sc.innerHTML += row;
        }
    });
    sc.addEventListener('change', () => {
        numca = sc.value;
        updateContent();
        updateSelectGastos();
        updateInputGastos();
    });
}

const guardar = async () => {
    const nameg = document.querySelector('#nombre-gasto').value;
    const montog = Number.parseFloat(document.querySelector('#monto-gasto').value);
    const fechag = document.querySelector('#fecha-gasto').value;

    if (montog <= 0) {
        alert('El monto del gasto debe ser mayor a 0.');
        return;
    }

    user.categories[numca].gastos.unshift({
        valor: montog,
        descripcion: nameg,
        fecha: fechag
    });

    data.users[id] = user;
    await guardar_data(data);
    alert('El gasto fue añadido exitosamente.');
}

const editar = async () => {
    const nameg = document.querySelector('#nuevo-nombre-gasto').value;
    const montog = Number.parseFloat(document.querySelector('#nuevo-monto-gasto').value);
    const fechag = document.querySelector('#nuevo-fecha-gasto').value;

    if (montog <= 0) {
        alert('El monto del gasto debe ser mayor a 0.');
        return;
    }

    user.categories[numca].gastos[numga].descripcion = nameg;
    user.categories[numca].gastos[numga].valor = montog;
    user.categories[numca].gastos[numga].fecha = fechag;

    data.users[id] = user;
    await guardar_data(data);
    alert('El gasto fue actualizado exitosamente.');
}

const eliminar = async () => {
    const desicion = confirm('Está seguro de eliminar el gasto ?');
    if (desicion === true) {
        user.categories[numca].gastos.splice(numga, 1);
        data.users[id] = user;
        await guardar_data(data);
        alert('El gasto fue eliminado satisfactoriamente.');
    }
}

selectOpcion.addEventListener('change', () => {
    opcion = selectOpcion.value;
    updateContent();
})

selectGastos.addEventListener('change', () => {
    numga = selectGastos.value;
    updateInputGastos();
});

updateContent();
updateSelectGastos();
updateInputGastos();
document.querySelector('#guardar').onclick = guardar;

