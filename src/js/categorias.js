import { verificar_categorias } from './logica.js';//extraemos los datos de los scripts como funciones y eso 
import { cargar_data, guardar_data } from './data.js';//nos sirve para cargar o extraer datos almacenados en data.js 

const data = await cargar_data(); //llamamos la funcion cargar data 
const params = new URLSearchParams(window.location.href.split('?')[1]);
const id = Number.parseInt(params.get('id'));
const user = data.users[id];
let nc = 0;

const links = document.getElementsByClassName('link');

for (const link of links) { //pasa el id del usuario los parametros a las otras paginas categorias o gastos para que se guarden 
    //PARA EL usuario en las otras paginas 
    link.href += `?id=${ id }`;
}

const checkEditar = document.getElementById('editar');//llamamos los checkbox editar y eliminar categorias
const checkEliminar = document.getElementById('eliminar');
const selectCategorias = document.getElementById('categorias');//llamamos el select categorias para editar o eliminar

user.categories.forEach((c, i) => { // para cada categoria comparar si ya existe 
    if (c.name.localeCompare('default') !== 0) {
        const row = `<option value=${ i }>${ c.name }</option>`;
        selectCategorias.innerHTML += row;
    }
});

const guardar = async () => {//guardar los elementos de la categoria 
    const namec = document.querySelector('#agregar').value;//llamamos la linea esa donde se escriben cosas para extraer su valor o contenido
    if (verificar_categorias(user.categories, namec) === true) {
        alert('La categoría ya se encuentra registrada.');//con la funcion verificar categorias vamos a ver si ya existe  mostrar .
    } else {
        user.categories.unshift({//crear categoria
            name: namec,// poner el nombre de la categoria
            gastos: []//crear el espacio para los gastos. 
        });
        data.users[id] = user;// almacenamos los datos al usuario. en data.js 
        await guardar_data(data);
        alert('Categoría agragada exitosamente.');
    }
    document.querySelector('#agregar').value = '';
};

const editar = async () => {// para editar 
    const nombre = document.querySelector('#nuevo').value;//extraemos el nuevo nombre de la categoria que se almacena en la linea "nuevo"
    if (verificar_categorias(user.categories, nombre) === true) {//se verifica que el nombre no sea el mismo 
        alert('La categoría ya se encuentra registrada.');// si es el mismo. 
    } else {
        user.categories[nc].name = nombre;
        data.users[id] = user;//Lo que hace es que el nombre ingresado por el input lo actualice en el usuario
        await guardar_data(data);
        alert('Nombre de categoría actualizado correctamente.');
    }
}

const eliminar = async () => {
    const decision = confirm('Desea eliminar los gastos asociados a esta categoría ?');//confirmacion para eliminar categoria
    if (decision) {
        user.categories.splice(nc, 1); //usamos splice para cambiar el contenido  de un array eliminando elementos existentes y/o agregando nuevos elementos
    } else {
        const gastos = user.categories[nc].gastos;
        user.categories[(user.categories.length - 1)].gastos.push(...gastos);
        user.categories.splice(nc, 1);
    }
    data.users[id] = user;
    await guardar_data(data);
    alert('Categoría eliminada satisfactoriamente.');
}

checkEditar.addEventListener('change', () => {//para el boton editar categoria
    console.log('editar')
    if(checkEditar.checked) {
        checkEliminar.checked = false;
        document.querySelector('#e-e-c').onclick = editar;
    }
});

checkEliminar.addEventListener('change', () => {
    if (checkEliminar.checked) {
        checkEditar.checked = false;
        document.querySelector('#e-e-c').onclick = eliminar; //boton editar o eliminar o categoria
    }
});

selectCategorias.addEventListener('change', () => {// para usar el select 
    nc = selectCategorias.value;
});

document.querySelector('#guardar').onclick = guardar; // boton guardar categoria
document.querySelector('#e-e-c').onclick = editar;//boton editar o eliminar categoria


