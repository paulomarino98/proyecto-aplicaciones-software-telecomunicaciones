import { cargar_data, guardar_data } from './data.js';
import { registrar_usuario, verificar_usuario } from './logica.js';

const data = await cargar_data();
const form = document.querySelector('#form');
const botoni = document.querySelector('#btn-i');
const botonr = document.querySelector('#btn-r');

const enviar = async () => {
    const email = document.querySelector('#email').value;
    const pass = document.querySelector('#pass').value;
    console.log(email, pass)
    const usuario = await verificar_usuario(data.users, email, pass);
    if (usuario.state === true) {
        const url = `${ window.location.href.split('views/')[0] }views/dashboard.html?id=${ usuario.index }`;
        form.action = url;
        form.submit();
    } else {
        alert(usuario.message);
    }
}

const registrar = async () => {
    const email = document.querySelector('#correo-r').value;
    const pass = document.querySelector('#pass-r').value;
    const usuario = await registrar_usuario(email, pass);
    data.users.unshift(usuario);
    await guardar_data(data);
    alert('Usuario registrado satisfactoriamente.');
}

botoni.onclick = enviar;
botonr.onclick = registrar;

form.addEventListener('submit', (e) => {
    e.preventDefault();
});


