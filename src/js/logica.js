const bcrypt = require('bcrypt'); // libreria para encripcion


/**
 * 
 * @param {Array} categorias El arreglo de categorías guardadas por el usuario
 * @param {any} nueva_categoria La categoría que el usuario desea ingresar
 * @returns Verdadero si la categoría ya está registrada, o falso si la categoría no está registrada
 */
export const verificar_categorias = (categorias, nueva_categoria) => { //verificamos si la longitud de las categorias es mayor 1
    if (categorias.length <= 1) {
        return false;
    }

    for (let c of categorias) { //comparamos si la categoria existe 
        if (c.name.localeCompare(nueva_categoria) === 0) {
            return true;
        }
    }

    return false;
}

/**
 * 
 * @param {string} email el email del usuario
 * @param {string} pass la contraseña sin encriptar del usuario
 * @returns el usuario registrado
 */
export const registrar_usuario = async (email, pass) => { //funcion registrar usuario 
    const encrypted = await encriptar_pass(pass); //usamos la funcion encriptar la pss(contraseña) para encriptar 
    const new_user = { //datos que vamos a almacenar en data.js
        email: email, 
        pass: encrypted,
        categories: [{
            name: "default",
            gastos: []
        }]
    };
    return new_user;
}

/**
 * 
 * @param {Array} users los usuarios registrados en el sistema 
 * @param {string} email el email del usuario que quiere iniciar sesión 
 * @param {string} pass la contraseña del usuario que quiere iniciar sesión
 * @returns null si el usuario no está registrado o la contraseña es incorrecta, el usuario si ingreso correctamente el correo y la contraseña
 */
export const verificar_usuario = async (users, email, pass) => {
    let user = null;
    let index = 0;
    users.forEach((u, i) => {
        if(u.email.localeCompare(email) === 0) {
            user = u;
            index = i;
        }
    });
    if (user !== null) {
        const result = await bcrypt.compare(pass, user.pass);
        if (result === true) {
            console.log();
            return {
                index: index,
                user: user,
                state: true,
                message: 'Sesión iniciada correctamente.'
            };
        } else {
            return {
                state: false,
                message: 'Correo o contraseña incorrectos.'
            };
        }
    }
    return {
        state: false,
        message: 'Usuario no registrado.'
    };
}

/**
 * 
 * @param {string} pass la contraseña a encriptar
 * @returns la contraseña encriptada
 */
const encriptar_pass = async (pass) => { //funcion encriptar contraseña
    const rounds = 10;
    const hash = await bcrypt.genSalt(rounds);
    const encrypt = bcrypt.hash(pass, hash);
    return encrypt;
}