import { cargar_data } from './data.js';

const data = await cargar_data();//trae la funcion para cargar los datos en datajs 
const params = new URLSearchParams(window.location.href.split('?')[1]); 
const id = 0; /*Number.parseInt(params.get('id'));*/
const user = data.users[id];

const links = document.getElementsByClassName('link');

for (const link of links) {
    link.href += `?id=${ id }`;
}

const categorias = document.querySelector('#rows-categorias');//tomamos las filas para insertar el datos
const gastos = document.querySelector('#gastos');

for (const c of user.categories) {
    if (c.name.localeCompare('default') !== 0) {
        let row = `<tr><td>${ c.name }</td></tr>`;
        categorias.innerHTML += row;

        for (const g of c.gastos) {
            row = `<tr>
                <td>${ c.name }</td>
                <td>${ g.descripcion }</td>
                <td>${ g.valor }</td>
                <td>${ g.fecha }</td>
            </tr>`
            gastos.innerHTML += row; //InnerHTML permite leer un dato o asignarlo al contenido de un div 
        }
    }
}





