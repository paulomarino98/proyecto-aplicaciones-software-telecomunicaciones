const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const usuario_schema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: [true, 'El correo es necesario.']
    },
    password: {
        type: String,
        required: [true, 'La contraseña es necesaria.']
    }
});
usuario_schema.method('compare_password', function (password = '') {
    if (bcrypt.compareSync(password, this.password)) {
        return true;
    }
    return false;
});
const Usuario = mongoose.model('usuario', usuario_schema);

module.exports = {
    Usuario
}
