const mongoose = require("mongoose");
const gasto_schema = new mongoose.Schema({
    valor: {
        type: Number,
        required: true,
        min: 0
    },
    descripcion: {
        type: String,
        required: true
    },
    fecha: {
        type: String,
        required: true
    },
    categoria: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Categoria',
        required: true
    }
});
const Gasto = mongoose.model('gasto', gasto_schema);

module.exports = {
    Gasto
}
