const mongoose = require("mongoose");
const categories_schema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    }
});
const Categoria = mongoose.model('categorias', categories_schema);

module.exports = {
    Categoria
}
