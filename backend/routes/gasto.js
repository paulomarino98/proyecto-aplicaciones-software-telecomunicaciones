const express = require("express");
const { Categoria } = require("../models/categoria.js");
const { Gasto } = require("../models/gasto.js");

const gasto_routes = express.Router();
gasto_routes.post('/crear', (req, res) => {
    const valor_gasto = req.body.valor_gasto;
    const desc_gasto = req.body.desc_gasto;
    const fecha_gasto = req.body.fecha_gasto;
    const id_categoria = req.body.id_categoria;
    Categoria.findById(id_categoria, (error, category_db) => {
        const gasto = {
            valor: valor_gasto,
            descripcion: desc_gasto,
            fecha: fecha_gasto,
            categoria: category_db
        };
        Gasto.create(gasto).then(gasto_db => {
            res.json({
                ok: true,
                gasto: gasto_db
            });
        });
    });
});
gasto_routes.post('/obtener', (req, res) => {
    const id_categoria = req.body.id_categoria;
    Categoria.findById(id_categoria, (error, category_db) => {
        Gasto.find({ categoria: category_db }, (error, gasto_db) => {
            res.json({
                ok: true,
                gastos: gasto_db
            });
        });
    });
});
gasto_routes.post('/editar', (req, res) => {
    const id_gasto = req.body.id_gasto;
    const valor_gasto = req.body.valor_gasto;
    const desc_gasto = req.body.desc_gasto;
    const fecha_gasto = req.body.fecha_gasto;
    const gasto = {
        valor: valor_gasto,
        descripcion: desc_gasto,
        fecha: fecha_gasto,
    };
    Gasto.findByIdAndUpdate(id_gasto, gasto, { new: true }, (error, gasto_db) => {
        return res.json({
            ok: true,
            gasto: gasto_db
        });
    });
});
gasto_routes.post('/obtener-uno', (req, res) => {
    const id_gasto = req.body.id_gasto;
    Gasto.findById(id_gasto, (error, gasto_db) => {
        res.json({
            ok: true,
            gasto: gasto_db
        });
    });
});
gasto_routes.post('/eliminar', (req, res) => {
    const id_gasto = req.body.id_gasto;
    Gasto.findByIdAndDelete(id_gasto, null, (error, gasto_db) => {
        return res.json({
            ok: true,
            gasto: gasto_db
        });
    });
});

module.exports = gasto_routes
