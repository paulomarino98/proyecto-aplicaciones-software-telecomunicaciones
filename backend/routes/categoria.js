const express = require("express");
const { Usuario } = require("../models/usuario.js");
const { Categoria } = require("../models/categoria.js");
const { Gasto } = require("../models/gasto.js");

const category_routes = express.Router();
category_routes.post('/crear', (req, res) => {
    const nombre = req.body.nombre;
    const id_user = req.body.id_user;
    Usuario.findById(id_user, (error, user_db) => {
        const categoria = {
            name: nombre,
            usuario: user_db
        };
        Categoria.create(categoria).then(category_db => {
            res.json({
                ok: true
            });
        });
    });
});
category_routes.post('/editar', (req, res) => {
    const nombre = req.body.nombre;
    const id_categoria = req.body.id_categoria;
    const categoria = {
        name: nombre
    };
    Categoria.findByIdAndUpdate(id_categoria, categoria, { new: true }, (error, category_db) => {
        return res.json({
            ok: true,
            categoria: category_db
        });
    });
});
category_routes.post('/eliminar', (req, res) => {
    const id_categoria = req.body.id_categoria;
    const id_usuario = req.body.id_usuario;
    const decision = req.body.decision;
    if (decision) {
        Categoria.findByIdAndDelete(id_categoria, null, (error, category_db) => {
            Gasto.deleteMany({ categoria: category_db }, (error1, gasto_db) => {
                return res.json({
                    ok: true,
                    categoria: category_db,
                    gasto: gasto_db
                });
            });
        });
    }
    else {
        Categoria.findByIdAndDelete(id_categoria, null, (error, category_db) => {
            Gasto.find({ categoria: category_db }, (error1, gasto_db) => {
                Usuario.findById(id_usuario, null, (error2, user_db) => {
                    Categoria.findOne({ usuario: user_db, name: 'default' }, (error3, dfcategory_db) => {
                        for (const gasto of gasto_db) {
                            Gasto.findByIdAndUpdate(gasto._id.toString(), { categoria: dfcategory_db }, (error4, gasto) => {
                            });
                        }
                        return res.json({
                            ok: true,
                            categoria: category_db
                        });
                    });
                });
            });
        });
    }
});
category_routes.post('/obtener', (req, res) => {
    const id_user = req.body.id_user;
    Usuario.findById(id_user, (error, user_db) => {
        Categoria.find({ usuario: user_db }, (error, category_db) => {
            res.json({
                ok: true,
                categorias: category_db
            });
        });
    });
});

module.exports = category_routes

