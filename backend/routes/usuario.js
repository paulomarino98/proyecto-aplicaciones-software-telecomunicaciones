const bcrypt = require("bcrypt");
const express = require("express");

const { Usuario } = require("../models/usuario.js");
const { Categoria } = require("../models/categoria.js");

const user_routes = express.Router();
user_routes.post('/login', (req, res) => {
    const user = {
        email: req.body.email,
        password: req.body.password
    };
    Usuario.findOne({ email: user.email }, (error, user_db) => {
        if (error) {
            throw error;
        }
        if (!user_db) {
            return res.json({
                ok: false,
                message: 'Email o Contraseña no son correctas'
            });
        }
        if (user_db.compare_password(user.password)) {
            return res.json({
                ok: true,
                message: 'Inicio de Sesión correcto.',
                id: user_db.id
            });
        }
        else {
            return res.json({
                ok: false,
                message: 'Email o Contraseña no son correctas'
            });
        }
    });
});
user_routes.post('/register', (req, res) => {
    const user = {
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10)
    };
    Usuario.create(user).then(user_db => {
        Categoria.create({
            name: 'default',
            usuario: user_db
        }).then(() => {
            res.json({
                ok: true,
                user: user_db
            });
        });
    }).catch(error => {
        res.json({
            ok: false,
            error: error
        });
    });
});
module.exports = user_routes;
