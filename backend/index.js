const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const category_routes = require("./routes/categoria.js");
const gasto_routes = require("./routes/gasto.js");
const user_routes = require("./routes/usuario.js");


// Conectar DB
mongoose.connect('mongodb://localhost:27017/FinanzasApp', (error) => {
    if (error) {
        throw error;
    }
    console.log('Base de Datos Online.');
});
// Levantar express
app.listen(3000, () => {
    console.log('Servidor corriendo en puerto 3000.');
});
